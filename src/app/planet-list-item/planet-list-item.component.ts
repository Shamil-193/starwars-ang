import { Component, Input, OnInit } from "@angular/core";
import { Planet } from "../interfaces/planet.interface";
import { HttpService } from "../services/http.service";

@Component({
  selector: "app-planet-list-item",
  templateUrl: "./planet-list-item.component.html",
  styleUrls: ["./planet-list-item.component.scss"]
})
export class PlanetListItemComponent implements OnInit {

  @Input() planet: Planet

  constructor(private httpService: HttpService) {}

  ngOnInit(): void {
  }

  wantVisit(name:string){
    this.httpService.wantVisit(name)
  }

  noVisit(name:string){
    this.httpService.notVisit(name)
  }

}
