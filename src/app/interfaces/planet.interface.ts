export interface Planet{
    name: string,
    population: number,
    diameter: number,
    visit?: boolean
}
export interface IResponse<T> {
    message: string;
    error: boolean;
    code: number;
    results: T;
    next: string | null
  }