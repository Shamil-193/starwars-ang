import { Component } from "@angular/core";
import { Planet } from "./interfaces/planet.interface";
import { HttpService } from "./services/http.service";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent {

  planetes: Planet[]
  title="StarWars-ang";

  constructor(private httpService: HttpService) {
    this.httpService.onPlanetesUpdate.subscribe(inpPlanetes=>this.planetes=inpPlanetes)
   }

}
