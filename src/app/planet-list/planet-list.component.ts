import { Component, OnInit } from "@angular/core";
import { Planet } from "../interfaces/planet.interface";
import { HttpService } from "../services/http.service";

@Component({
  selector: "app-planet-list",
  templateUrl: "./planet-list.component.html",
  styleUrls: ["./planet-list.component.scss"]
})
export class PlanetListComponent implements OnInit {

  loading:boolean=true

  planetes: Planet[]=[]

  constructor(private httpService:HttpService) {
    this.httpService.onPlanetesUpdate.subscribe(inpPlanetes =>{
      this.planetes=inpPlanetes
    })
    this.httpService.onLoadingDone.subscribe(inpstat=>this.loading=inpstat)
   }

  ngOnInit(): void {
    this.fetchPlanetes("https://swapi.dev/api/planets")
  }

  fetchPlanetes(url:string): void{
    this.loading=true
    this.httpService.fetchPlanetes(url)
  }

}
