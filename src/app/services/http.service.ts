import { HttpClient } from "@angular/common/http";
import { EventEmitter, Injectable } from "@angular/core";
import { IResponse, Planet } from "../interfaces/planet.interface";

@Injectable({
  providedIn: "root"
})
export class HttpService {

  planetes: Planet[]=[]

  onPlanetesUpdate: EventEmitter<Planet[]>=new EventEmitter()
  onLoadingDone: EventEmitter<boolean>=new EventEmitter()

  constructor(private http: HttpClient) { }

  fetchPlanetes(url:string){
    this.http.get<IResponse<Planet[]>>(url).subscribe(inpPlanetes => {
        this.planetes.push(...inpPlanetes.results)
        console.log(inpPlanetes.results)
        if(inpPlanetes.next){
          this.onPlanetesUpdate.emit(this.planetes)
          this.fetchPlanetes(inpPlanetes.next) 
        }else{
          this.onLoadingDone.emit(false)
          console.log("status")
        }
      })
    }

  wantVisit(inpName:string){
    this.planetes?.map(value => value.name==inpName ? value.visit=true : value);
    console.log("planetes", this.planetes);
    this.onPlanetesUpdate.emit(this.planetes);
  }

  notVisit(inpName:string){
    this.planetes?.map(value => value.name==inpName ? value.visit=false : value);
    console.log("planetes", this.planetes);
    this.onPlanetesUpdate.emit(this.planetes);
  }

  setPlanetes(inpPlannetes:Planet[]){
    this.planetes=inpPlannetes
    console.log("set planetes")
    this.onPlanetesUpdate.emit(this.planetes)
  }
}
